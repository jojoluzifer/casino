jQuery(function(){
	jQuery("#image-carousel").carouFredSel({
		auto : {
			play: 			true,
			pauseDuration: 	3000,
			pauseOnHover:	true
		},
		scroll : {
			easing : "easeOutSine",
			duration: 1000
		},
		items: 1,
		direction: "left",
		prev : {   
			button  : "#web-image-carousel-prev"
		},
		next : {
			button  : "#web-image-carousel-next"
		}
	});	
});
